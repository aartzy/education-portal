import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StudentGroupUpdatedEvent } from './student-group-updated.event';
import { StudentGroupService } from '../../entity/student-group/student-group.service';

@EventsHandler(StudentGroupUpdatedEvent)
export class StudentGroupUpdatedCommandHandler
  implements IEventHandler<StudentGroupUpdatedEvent> {
  constructor(private readonly object: StudentGroupService) {}

  async handle(event: StudentGroupUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
