import { IQuery } from '@nestjs/cqrs';

export class RetrieveStudentGroupQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
