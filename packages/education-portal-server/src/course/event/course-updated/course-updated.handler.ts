import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseUpdatedEvent } from './course-updated.event';
import { CourseService } from '../../entity/course/course.service';

@EventsHandler(CourseUpdatedEvent)
export class CourseUpdatedEventHandler
  implements IEventHandler<CourseUpdatedEvent> {
  constructor(private readonly object: CourseService) {}

  async handle(event: CourseUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { course_name: updatePayload.course_name },
      { $set: updatePayload },
    );
  }
}
