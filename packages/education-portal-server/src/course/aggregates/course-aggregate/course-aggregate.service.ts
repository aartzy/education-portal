import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { CourseDto } from '../../entity/course/course-dto';
import { Course } from '../../entity/course/course.entity';
import { CourseAddedEvent } from '../../event/course-added/course-added.event';
import { CourseService } from '../../entity/course/course.service';
import { CourseRemovedEvent } from '../../event/course-removed/course-removed.event';
import { CourseUpdatedEvent } from '../../event/course-updated/course-updated.event';
import { UpdateCourseDto } from '../../entity/course/update-course-dto';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { switchMap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';

@Injectable()
export class CourseAggregateService extends AggregateRoot {
  constructor(
    private readonly courseService: CourseService,
    private readonly teacherService: TeacherService,
  ) {
    super();
  }

  addCourse(coursePayload: CourseDto, clientHttpRequest) {
    const course = new Course();
    Object.assign(course, coursePayload);
    course.uuid = uuidv4();
    this.apply(new CourseAddedEvent(course, clientHttpRequest));
  }

  async retrieveCourse(getCoursePayload: GetCourseQueryDto, clientHttpRequest) {
    return this.teacherService
      .asyncAggregate([
        {
          $match: { instructor_email: clientHttpRequest.token.email },
        },
        { $unwind: '$instructor_log' },
        {
          $lookup: {
            from: 'course',
            localField: 'instructor_log.course',
            foreignField: 'name',
            as: 'topics',
          },
        },
        { $unwind: '$topics' },
        {
          $match: {
            'instructor_log.program': getCoursePayload.program_name,
            'topics.course_name': getCoursePayload.course_name,
          },
        },
        {
          $project: {
            instructor_name: 1,
            program: '$instructor_log.program',
            course_name: '$topics.course_name',
            topics: '$topics.topics',
          },
        },
      ])
      .pipe(
        switchMap((course: any[]) => {
          if (!course || !course.length) {
            return throwError(new NotFoundException());
          }
          return of(course);
        }),
      );
  }

  async getCourseList(
    offset: number,
    limit: number,
    sort: number,
    search: string,
    clientHttpRequest,
  ) {
    return this.teacherService
      .asyncAggregate([
        {
          $match: { instructor_email: clientHttpRequest.token.email },
        },
        { $unwind: '$instructor_log' },
        {
          $lookup: {
            from: 'course',
            localField: 'instructor_log.course',
            foreignField: 'name',
            as: 'topics',
          },
        },
        { $unwind: '$topics' },
        {
          $project: {
            instructor_name: 1,
            program: '$instructor_log.program',
            course_name: '$topics.course_name',
            topics: '$topics.topics',
          },
        },
        { $limit: limit },
        { $skip: offset },
        { $sort: { course_name: sort } },
      ])
      .pipe(
        switchMap((course: any[]) => {
          if (!course || !course.length) {
            return throwError(new NotFoundException());
          }
          return of(course);
        }),
      );
  }
  async remove(uuid: string) {
    const found = await this.courseService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new CourseRemovedEvent(found));
  }
  async update(updatePayload: UpdateCourseDto) {
    const provider = await this.courseService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new CourseUpdatedEvent(update));
  }
}
