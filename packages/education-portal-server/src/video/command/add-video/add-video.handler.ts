import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddVideoCommand } from './add-video.command';
import { VideoAggregateService } from '../../aggregates/video-aggregate/video-aggregate.service';

@CommandHandler(AddVideoCommand)
export class AddVideoCommandHandler
  implements ICommandHandler<AddVideoCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: VideoAggregateService,
  ) {}
  async execute(command: AddVideoCommand) {
    const { videoPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addVideo(videoPayload, clientHttpRequest);
    aggregate.commit();
  }
}
