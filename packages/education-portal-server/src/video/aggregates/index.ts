import { VideoAggregateService } from './video-aggregate/video-aggregate.service';
import { VideoWebhookAggregateService } from './video-webhook-aggregate/video-webhook-aggregate.service';

export const VideoAggregatesManager = [
  VideoAggregateService,
  VideoWebhookAggregateService,
];
