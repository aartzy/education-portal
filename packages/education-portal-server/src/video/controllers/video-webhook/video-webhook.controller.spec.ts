import { Test, TestingModule } from '@nestjs/testing';
import { VideoWebhookController } from './video-webhook.controller';
import { VideoWebhookAggregateService } from '../../../video/aggregates/video-webhook-aggregate/video-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('VideoWebhook Controller', () => {
  let controller: VideoWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: VideoWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [VideoWebhookController],
    }).compile();

    controller = module.get<VideoWebhookController>(VideoWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
