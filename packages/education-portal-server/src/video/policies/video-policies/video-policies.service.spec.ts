import { Test, TestingModule } from '@nestjs/testing';
import { VideoPoliciesService } from './video-policies.service';

describe('VideoPoliciesService', () => {
  let service: VideoPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VideoPoliciesService],
    }).compile();

    service = module.get<VideoPoliciesService>(VideoPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
