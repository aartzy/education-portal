import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Video } from './video/video.entity';
import { VideoService } from './video/video.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Video]), CqrsModule],
  providers: [VideoService],
  exports: [VideoService],
})
export class VideoEntitiesModule {}
