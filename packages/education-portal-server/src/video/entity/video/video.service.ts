import { InjectRepository } from '@nestjs/typeorm';
import { Video } from './video.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class VideoService {
  constructor(
    @InjectRepository(Video)
    private readonly videoRepository: MongoRepository<Video>,
  ) {}

  async find(query?) {
    return await this.videoRepository.find(query);
  }

  async create(video: Video) {
    const videoObject = new Video();
    Object.assign(videoObject, video);
    return await this.videoRepository.insertOne(videoObject);
  }

  async findOne(param, options?) {
    return await this.videoRepository.findOne(param, options);
  }

  async list(skip, take, search, sort, videoList) {
    const sortQuery = { name: sort };
    const nameExp = new RegExp(search, 'i');
    const columns = this.videoRepository.manager.connection
      .getMetadata(Video)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });

    const videoQuery = { name: { $in: videoList } };

    const $and: any[] = [{ $or }, videoQuery];

    const where: { $and: any } = { $and };

    const results = await this.videoRepository.find({
      skip,
      take,
      where,
      order: sortQuery,
    });

    return {
      docs: results || [],
      length: await this.videoRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.videoRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.videoRepository.updateOne(query, options);
  }
}
