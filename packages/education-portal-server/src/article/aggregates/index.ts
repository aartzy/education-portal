import { ArticleAggregateService } from './article-aggregate/article-aggregate.service';
import { ArticleWebhookAggregateService } from './article-webhook-aggregate/article-webhook-aggregate.service';

export const ArticleAggregatesManager = [
  ArticleAggregateService,
  ArticleWebhookAggregateService,
];
