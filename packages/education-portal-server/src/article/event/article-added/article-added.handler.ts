import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ArticleAddedEvent } from './article-added.event';
import { ArticleService } from '../../entity/article/article.service';

@EventsHandler(ArticleAddedEvent)
export class ArticleAddedCommandHandler
  implements IEventHandler<ArticleAddedEvent> {
  constructor(private readonly articleService: ArticleService) {}
  async handle(event: ArticleAddedEvent) {
    const { article } = event;
    await this.articleService.create(article);
  }
}
