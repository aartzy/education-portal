import { IEvent } from '@nestjs/cqrs';
import { Article } from '../../entity/article/article.entity';

export class ArticleAddedEvent implements IEvent {
  constructor(public article: Article, public clientHttpRequest: any) {}
}
