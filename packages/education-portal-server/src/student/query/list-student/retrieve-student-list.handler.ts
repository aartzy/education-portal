import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveStudentListQuery } from './retrieve-student-list.query';
import { StudentAggregateService } from '../../aggregates/student-aggregate/student-aggregate.service';

@QueryHandler(RetrieveStudentListQuery)
export class RetrieveStudentListQueryHandler
  implements IQueryHandler<RetrieveStudentListQuery> {
  constructor(private readonly manager: StudentAggregateService) {}
  async execute(query: RetrieveStudentListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getStudentList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
