import { Test, TestingModule } from '@nestjs/testing';
import { StudentWebhookController } from './student-webhook.controller';
import { StudentWebhookAggregateService } from '../../../student/aggregates/student-webhook-aggregate/student-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('StudentWebhook Controller', () => {
  let controller: StudentWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: StudentWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [StudentWebhookController],
    }).compile();

    controller = module.get<StudentWebhookController>(StudentWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
