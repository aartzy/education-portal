import { Test, TestingModule } from '@nestjs/testing';
import { StudentAggregateService } from './student-aggregate.service';
import { StudentService } from '../../../student/entity/student/student.service';
describe('generaCqrslAggregateService', () => {
  let service: StudentAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentAggregateService,
        {
          provide: StudentService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StudentAggregateService>(StudentAggregateService);
  });
  StudentAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
