import { Injectable, BadRequestException } from '@nestjs/common';
import { StudentWebhookDto } from '../../entity/student/student.webhook-dto';
import { StudentService } from '../../../student/entity/student/student.service';
import { Student } from '../../../student/entity/student/student.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { STUDENT_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class StudentWebhookAggregateService {
  constructor(private readonly studentService: StudentService) {}

  studentCreated(studentPayload: StudentWebhookDto) {
    return from(
      this.studentService.findOne({
        name: studentPayload.name,
      }),
    ).pipe(
      switchMap(student => {
        if (student) {
          return throwError(new BadRequestException(STUDENT_ALREADY_EXISTS));
        }
        const provider = this.mapStudent(studentPayload);
        return from(this.studentService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapStudent(studentPayload: StudentWebhookDto) {
    const student = new Student();
    Object.assign(student, studentPayload);
    student.isSynced = true;
    student.uuid = uuidv4();
    return student;
  }

  studentDeleted(studentPayload: StudentWebhookDto) {
    this.studentService.deleteOne({ name: studentPayload.name });
  }

  studentUpdated(studentPayload: StudentWebhookDto) {
    return from(
      this.studentService.findOne({ name: studentPayload.name }),
    ).pipe(
      switchMap(student => {
        if (!student) {
          return this.studentCreated(studentPayload);
        }
        student.isSynced = true;
        return from(
          this.studentService.updateOne(
            { name: student.name },
            { $set: studentPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
