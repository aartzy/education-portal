import { RetrieveAttendanceQueryHandler } from './get-attendance/retrieve-attendance.handler';
import { RetrieveAttendanceListQueryHandler } from './list-attendance/retrieve-attendance-list.handler';

export const AttendanceQueryManager = [
  RetrieveAttendanceQueryHandler,
  RetrieveAttendanceListQueryHandler,
];
