import { Test, TestingModule } from '@nestjs/testing';
import { AttendanceWebhookController } from './attendance-webhook.controller';
import { AttendanceWebhookAggregateService } from '../../../attendance/aggregates/attendance-webhook-aggregate/attendance-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('AttendanceWebhook Controller', () => {
  let controller: AttendanceWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: AttendanceWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [AttendanceWebhookController],
    }).compile();

    controller = module.get<AttendanceWebhookController>(
      AttendanceWebhookController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
