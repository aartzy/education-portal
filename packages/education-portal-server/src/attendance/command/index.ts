import { AddAttendanceCommandHandler } from './add-attendance/add-attendance.handler';
import { RemoveAttendanceCommandHandler } from './remove-attendance/remove-attendance.handler';
import { UpdateAttendanceCommandHandler } from './update-attendance/update-attendance.handler';

export const AttendanceCommandManager = [
  AddAttendanceCommandHandler,
  RemoveAttendanceCommandHandler,
  UpdateAttendanceCommandHandler,
];
