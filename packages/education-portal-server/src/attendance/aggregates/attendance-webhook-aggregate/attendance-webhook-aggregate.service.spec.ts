import { Test, TestingModule } from '@nestjs/testing';
import { AttendanceWebhookAggregateService } from './attendance-webhook-aggregate.service';
import { AttendanceService } from '../../../attendance/entity/attendance/attendance.service';

describe('AttendanceWebhookAggregateService', () => {
  let service: AttendanceWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AttendanceWebhookAggregateService,
        {
          provide: AttendanceService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AttendanceWebhookAggregateService>(
      AttendanceWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
