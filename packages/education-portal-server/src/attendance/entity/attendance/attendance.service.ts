import { InjectRepository } from '@nestjs/typeorm';
import { Attendance } from './attendance.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class AttendanceService {
  constructor(
    @InjectRepository(Attendance)
    private readonly attendanceRepository: MongoRepository<Attendance>,
  ) {}

  async find(query?) {
    return await this.attendanceRepository.find(query);
  }

  async create(attendance: Attendance) {
    const attendanceObject = new Attendance();
    Object.assign(attendanceObject, attendance);
    return await this.attendanceRepository.insertOne(attendanceObject);
  }

  async findOne(param, options?) {
    return await this.attendanceRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.attendanceRepository.manager.connection
      .getMetadata(Attendance)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.attendanceRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.attendanceRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.attendanceRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.attendanceRepository.updateOne(query, options);
  }
}
