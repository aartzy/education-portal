import { Test, TestingModule } from '@nestjs/testing';
import { TeacherWebhookAggregateService } from './teacher-webhook-aggregate.service';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('TeacherWebhookAggregateService', () => {
  let service: TeacherWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TeacherWebhookAggregateService,
        {
          provide: TeacherService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TeacherWebhookAggregateService>(
      TeacherWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
