import {
  Injectable,
  NotFoundException,
  NotImplementedException,
  HttpService,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { TeacherDto } from '../../entity/teacher/teacher-dto';
import { Teacher } from '../../entity/teacher/teacher.entity';
import { TeacherAddedEvent } from '../../event/teacher-added/teacher-added.event';
import { TeacherService } from '../../entity/teacher/teacher.service';
import { TeacherRemovedEvent } from '../../event/teacher-removed/teacher-removed.event';
import { TeacherUpdatedEvent } from '../../event/teacher-updated/teacher-updated.event';
import { UpdateTeacherDto } from '../../entity/teacher/update-teacher-dto';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { switchMap, map, catchError } from 'rxjs/operators';
import { throwError, of, from } from 'rxjs';
import { FRAPPE_API_GET_COURSE_SCHEDULE_ENDPOINT } from '../../../constants/routes';
import { RoomService } from '../../../room/entity/room/room.service';
import { TEACHER_SCHEDULE_FIELDS } from '../../../constants/app-strings';
import { ScheduleDateDto } from '../../../teacher/entity/teacher/schedule-date-dto';
import { TEACHER_NOT_FOUND } from '../../../constants/messages';

@Injectable()
export class TeacherAggregateService extends AggregateRoot {
  constructor(
    private readonly teacherService: TeacherService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
    private readonly room: RoomService,
  ) {
    super();
  }

  addTeacher(
    studentPayload: TeacherDto,
    clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    const teacher = new Teacher();
    Object.assign(teacher, studentPayload);
    teacher.uuid = uuidv4();
    this.apply(new TeacherAddedEvent(teacher, clientHttpRequest));
  }

  async retrieveTeacher(uuid: string, req: ClientHttpRequestTokenInterface) {
    const provider = await this.teacherService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getTeacherList(
    offset,
    limit,
    sort,
    search,
    clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    return await this.teacherService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.teacherService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new TeacherRemovedEvent(found));
  }

  async update(updatePayload: UpdateTeacherDto) {
    const provider = await this.teacherService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new TeacherUpdatedEvent(update));
  }

  getTeacherSchedule(
    limit,
    offset,
    search,
    sort,
    datePayload: ScheduleDateDto,
    clientHttpRequest,
  ) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.filterUrl(
          clientHttpRequest,
          datePayload.from_date,
          datePayload.to_date,
        ).pipe(
          switchMap(filterUrl => {
            const url = `${settings.authServerURL}${FRAPPE_API_GET_COURSE_SCHEDULE_ENDPOINT}${filterUrl}`;
            const headers = {
              headers: this.settings.getAuthorizationHeaders(
                clientHttpRequest.token,
              ),
            };
            return this.http.get(url, headers).pipe(
              map(res => res.data.data),
              switchMap(res => {
                return this.roomNumberAggregation(res, offset, limit).pipe(
                  switchMap(courseSchedule => {
                    return of(courseSchedule);
                  }),
                );
              }),
              catchError(err => {
                return throwError(
                  new BadRequestException(this.settings.getErrorMessage(err)),
                );
              }),
            );
          }),
        );
      }),
    );
  }

  roomNumberAggregation(res, offset, limit) {
    return this.room
      .asyncAggregate([
        {
          $project: {
            name: 1,
            room_number: 1,
            data: {
              $filter: {
                input: res,
                as: 'res',
                cond: { $eq: ['$$res.room', '$name'] },
              },
            },
          },
        },
        {
          $unwind: '$data',
        },
        {
          $project: {
            schedule_date: '$data.schedule_date',
            to_time: '$data.to_time',
            from_time: '$data.from_time',
            instructor_name: '$data.instructor_name',
            course: '$data.course',
            room: '$data.room',
            room_no: '$room_number',
          },
        },
        { $skip: offset },
        { $limit: limit },
      ])
      .pipe(
        switchMap((course_schedule: any[]) => {
          if (!course_schedule || !course_schedule.length) {
            return of([]);
          }
          return of(course_schedule);
        }),
      );
  }

  filterUrl(clientHttpRequest, from_date, to_date) {
    return from(
      this.teacherService.findOne({
        instructor_email: clientHttpRequest.token.email,
      }),
    ).pipe(
      switchMap(teacher => {
        if (!teacher) {
          return throwError(new NotFoundException(TEACHER_NOT_FOUND));
        }
        const filter = `${TEACHER_SCHEDULE_FIELDS}${teacher.instructor_name}"],["schedule_date","between",["${from_date}","${to_date}"]]]`;
        return of(filter);
      }),
    );
  }
}
