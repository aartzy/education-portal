import { IEvent } from '@nestjs/cqrs';
import { Teacher } from '../../entity/teacher/teacher.entity';

export class TeacherUpdatedEvent implements IEvent {
  constructor(public updatePayload: Teacher) {}
}
