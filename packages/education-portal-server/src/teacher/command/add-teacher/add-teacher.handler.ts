import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddTeacherCommand } from './add-teacher.command';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';

@CommandHandler(AddTeacherCommand)
export class AddTeacherCommandHandler
  implements ICommandHandler<AddTeacherCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TeacherAggregateService,
  ) {}
  async execute(command: AddTeacherCommand) {
    const { teacherPayload: teacherPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addTeacher(teacherPayload, clientHttpRequest);
    aggregate.commit();
  }
}
