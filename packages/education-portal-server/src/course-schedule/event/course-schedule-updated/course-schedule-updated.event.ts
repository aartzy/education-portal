import { IEvent } from '@nestjs/cqrs';
import { CourseSchedule } from '../../entity/course-schedule/course-schedule.entity';

export class CourseScheduleUpdatedEvent implements IEvent {
  constructor(public updatePayload: CourseSchedule) {}
}
