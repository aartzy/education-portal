import { IEvent } from '@nestjs/cqrs';
import { CourseSchedule } from '../../entity/course-schedule/course-schedule.entity';

export class CourseScheduleAddedEvent implements IEvent {
  constructor(
    public courseSchedule: CourseSchedule,
    public clientHttpRequest: any,
  ) {}
}
