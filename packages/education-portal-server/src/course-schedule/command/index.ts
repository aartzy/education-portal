import { AddCourseScheduleCommandHandler } from './add-course-schedule/add-course-schedule.handler';
import { RemoveCourseScheduleCommandHandler } from './remove-course-schedule/remove-course-schedule.handler';
import { UpdateCourseScheduleCommandHandler } from './update-course-schedule/update-course-schedule.handler';

export const CourseScheduleCommandManager = [
  AddCourseScheduleCommandHandler,
  RemoveCourseScheduleCommandHandler,
  UpdateCourseScheduleCommandHandler,
];
