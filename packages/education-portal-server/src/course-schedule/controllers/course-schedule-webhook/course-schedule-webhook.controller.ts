import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { CourseScheduleWebhookDto } from '../../entity/course-schedule/course-schedule-webhook-dto';
import { CourseScheduleWebhookAggregateService } from '../../aggregates/course-schedule-webhook-aggregate/course-schedule-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('course_schedule')
export class CourseScheduleWebhookController {
  constructor(
    private readonly courseScheduleWebhookAggreagte: CourseScheduleWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  courseScheduleCreated(
    @Body() courseSchedulePayload: CourseScheduleWebhookDto,
  ) {
    return this.courseScheduleWebhookAggreagte.courseScheduleCreated(
      courseSchedulePayload,
    );
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  courseScheduleUpdated(
    @Body() courseSchedulePayload: CourseScheduleWebhookDto,
  ) {
    return this.courseScheduleWebhookAggreagte.courseScheduleUpdated(
      courseSchedulePayload,
    );
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  courseScheduleDeleted(
    @Body() courseSchedulePayload: CourseScheduleWebhookDto,
  ) {
    return this.courseScheduleWebhookAggreagte.courseScheduleDeleted(
      courseSchedulePayload,
    );
  }
}
