import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class QuestionWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  question: string;

  @IsOptional()
  @IsString()
  question_type: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => OptionsWebhookDto)
  options: OptionsWebhookDto[];
}
export class OptionsWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  option: string;

  @IsOptional()
  @IsNumber()
  is_correct: number;

  @IsOptional()
  @IsString()
  doctype: string;
}
