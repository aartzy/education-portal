import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Question extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  question: string;

  @Column()
  question_type: string;

  @Column()
  doctype: string;

  @Column()
  options: Options[];

  @Column()
  isSynced: boolean;
}
export class Options {
  name: string;
  docstatus: number;
  option: string;
  is_correct: number;
  doctype: string;
}
