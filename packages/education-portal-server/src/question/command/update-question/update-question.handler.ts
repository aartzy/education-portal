import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateQuestionCommand } from './update-question.command';
import { QuestionAggregateService } from '../../aggregates/question-aggregate/question-aggregate.service';
@CommandHandler(UpdateQuestionCommand)
export class UpdateQuestionCommandHandler
  implements ICommandHandler<UpdateQuestionCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuestionAggregateService,
  ) {}

  async execute(command: UpdateQuestionCommand) {
    const { updatePayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateQuestion(updatePayload, req).toPromise();
    aggregate.commit();
  }
}
