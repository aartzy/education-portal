import { Test, TestingModule } from '@nestjs/testing';
import { RoomWebhookAggregateService } from './room-webhook-aggregate.service';
import { RoomService } from '../../../room/entity/room/room.service';

describe('RoomWebhookAggregateService', () => {
  let service: RoomWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoomWebhookAggregateService,
        {
          provide: RoomService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<RoomWebhookAggregateService>(
      RoomWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
