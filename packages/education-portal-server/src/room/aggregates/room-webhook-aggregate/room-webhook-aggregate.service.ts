import { Injectable, BadRequestException } from '@nestjs/common';
import { RoomWebhookDto } from '../../entity/room/room-webhook-dto';
import { RoomService } from '../../../room/entity/room/room.service';
import { Room } from '../../../room/entity/room/room.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ROOM_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';

@Injectable()
export class RoomWebhookAggregateService {
  constructor(private readonly roomService: RoomService) {}

  roomCreated(roomPayload: RoomWebhookDto) {
    return from(
      this.roomService.findOne({
        name: roomPayload.name,
      }),
    ).pipe(
      switchMap(room => {
        if (room) {
          return throwError(new BadRequestException(ROOM_ALREADY_EXISTS));
        }
        const provider = this.mapRoom(roomPayload);
        return from(this.roomService.create(provider)).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }

  mapRoom(roomPayload: RoomWebhookDto) {
    const room = new Room();
    Object.assign(room, roomPayload);
    room.uuid = uuidv4();
    room.isSynced = true;
    return room;
  }

  roomDeleted(roomPayload: RoomWebhookDto) {
    this.roomService.deleteOne({ name: roomPayload.name });
  }

  roomUpdated(roomPayload: RoomWebhookDto) {
    return from(this.roomService.findOne({ name: roomPayload.name })).pipe(
      switchMap(room => {
        if (!room) {
          return this.roomCreated(roomPayload);
        }
        room.isSynced = true;
        return from(
          this.roomService.updateOne(
            { name: room.name },
            { $set: roomPayload },
          ),
        ).pipe(
          switchMap(() => {
            return of({});
          }),
        );
      }),
    );
  }
}
