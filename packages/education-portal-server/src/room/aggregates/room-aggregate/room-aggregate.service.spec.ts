import { Test, TestingModule } from '@nestjs/testing';
import { RoomAggregateService } from './room-aggregate.service';
import { RoomService } from '../../entity/room/room.service';

describe('RoomAggregateService', () => {
  let service: RoomAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoomAggregateService,
        {
          provide: RoomService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<RoomAggregateService>(RoomAggregateService);
  });
  RoomAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
