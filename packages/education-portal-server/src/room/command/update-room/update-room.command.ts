import { ICommand } from '@nestjs/cqrs';
import { UpdateRoomDto } from '../../entity/room/update-room-dto';

export class UpdateRoomCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateRoomDto) {}
}
