import { IEvent } from '@nestjs/cqrs';
import { Room } from '../../entity/room/room.entity';

export class RoomUpdatedEvent implements IEvent {
  constructor(public updatePayload: Room) {}
}
