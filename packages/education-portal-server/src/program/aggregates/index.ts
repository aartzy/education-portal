import { ProgramAggregateService } from './program-aggregate/program-aggregate.service';
import { ProgramWebhookAggregateService } from './program-webhook-aggregate/program-webhook-aggregate.service';

export const ProgramAggregatesManager = [
  ProgramAggregateService,
  ProgramWebhookAggregateService,
];
