import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ProgramDto } from '../../entity/program/program-dto';
import { AddProgramCommand } from '../../command/add-program/add-program.command';
import { RemoveProgramCommand } from '../../command/remove-program/remove-program.command';
import { UpdateProgramCommand } from '../../command/update-program/update-program.command';
import { RetrieveProgramQuery } from '../../query/get-program/retrieve-program.query';
import { RetrieveProgramListQuery } from '../../query/list-program/retrieve-program-list.query';
import { UpdateProgramDto } from '../../entity/program/update-program-dto';

@Controller('program')
export class ProgramController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() programPayload: ProgramDto, @Req() req) {
    return this.commandBus.execute(new AddProgramCommand(programPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveProgramCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveProgramQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveProgramListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateProgramDto) {
    return this.commandBus.execute(new UpdateProgramCommand(updatePayload));
  }
}
