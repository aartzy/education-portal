import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { DepartmentWebhookDto } from '../../entity/department/department-webhook-dto';
import { DepartmentWebhookAggregateService } from '../../aggregates/department-webhook-aggregate/department-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('department')
export class DepartmentWebhookController {
  constructor(
    private readonly departmentWebhookAggreagte: DepartmentWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  departmentCreated(@Body() departmentPayload: DepartmentWebhookDto) {
    return this.departmentWebhookAggreagte.departmentCreated(departmentPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  departmentUpdated(@Body() departmentPayload: DepartmentWebhookDto) {
    return this.departmentWebhookAggreagte.departmentUpdated(departmentPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  departmentDeleted(@Body() departmentPayload: DepartmentWebhookDto) {
    return this.departmentWebhookAggreagte.departmentDeleted(departmentPayload);
  }
}
