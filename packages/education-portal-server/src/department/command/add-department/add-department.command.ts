import { ICommand } from '@nestjs/cqrs';
import { DepartmentDto } from '../../entity/department/department-dto';

export class AddDepartmentCommand implements ICommand {
  constructor(
    public departmentPayload: DepartmentDto,
    public readonly clientHttpRequest: any,
  ) {}
}
