import { Module, HttpModule } from '@nestjs/common';
import { QuizAggregatesManager } from './aggregates';
import { QuizEntitiesModule } from './entity/entity.module';
import { QuizQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { QuizCommandManager } from './command';
import { QuizEventManager } from './event';
import { QuizController } from './controllers/quiz/quiz.controller';
import { QuizPoliciesService } from './policies/quiz-policies/quiz-policies.service';
import { QuizWebhookController } from './controllers/quiz-webhook/quiz-webhook.controller';
import { TopicModule } from '../topic/topic.module';
import { QuestionModule } from '../question/question.module';
import { TeacherModule } from '../teacher/teacher.module';
import { CourseModule } from '../course/course.module';

@Module({
  imports: [
    QuizEntitiesModule,
    CqrsModule,
    HttpModule,
    TopicModule,
    QuestionModule,
    TeacherModule,
    CourseModule,
  ],
  controllers: [QuizController, QuizWebhookController],
  providers: [
    ...QuizAggregatesManager,
    ...QuizQueryManager,
    ...QuizEventManager,
    ...QuizCommandManager,
    QuizPoliciesService,
  ],
  exports: [QuizEntitiesModule],
})
export class QuizModule {}
