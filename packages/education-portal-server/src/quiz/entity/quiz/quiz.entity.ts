import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Quiz extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  title: string;

  @Column()
  passing_score: number;

  @Column()
  max_attempts: number;

  @Column()
  grading_basis: string;

  @Column()
  doctype: string;

  @Column()
  question: Question[];

  @Column()
  isSynced: boolean;
}
export class Question {
  name: string;
  docstatus: number;
  question_link: string;
  question: string;
  doctype: string;
}
