import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class QuizDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsNumber()
  passing_score: number;

  @IsOptional()
  @IsNumber()
  max_attempts: number;

  @IsOptional()
  @IsString()
  grading_basis: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => QuizQuestionWebhookDto)
  question: string;
}
export class QuizQuestionWebhookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  question_link: string;

  @IsOptional()
  @IsString()
  question: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
