import { QuizAggregateService } from './quiz-aggregate/quiz-aggregate.service';
import { QuizWebhookAggregateService } from './quiz-webhook-aggregate/quiz-webhook-aggregate.service';

export const QuizAggregatesManager = [
  QuizAggregateService,
  QuizWebhookAggregateService,
];
