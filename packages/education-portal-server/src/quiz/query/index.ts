import { RetrieveQuizQueryHandler } from './get-quiz/retrieve-quiz.handler';
import { RetrieveQuizListQueryHandler } from './list-quiz/retrieve-quiz-list.handler';

export const QuizQueryManager = [
  RetrieveQuizQueryHandler,
  RetrieveQuizListQueryHandler,
];
